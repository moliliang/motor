# Motor

[![Build Status](https://travis-ci.org/0x1A/motor.png?branch=master)](https://travis-ci.org/0x1A/motor)
[![Coverage Status](https://coveralls.io/repos/0x1A/motor/badge.png?branch=master)](https://coveralls.io/r/0x1A/motor?branch=master)

Motor is a 2D game engine using SDL2 that is still very early in development.

### Building the current tests

To build the current test bed you must first solve dependencies. They can be solved by running `./bin/motorwork --install`, which solves dependencies by building local copies. For more information see the bin [README](https://github.com/0x1A/motor/tree/master/bin).

* Running: `make` results in the `MotorTest` bin, which has `Esc` set as the exit key.

* Running: `make timedtest` results in the `TimedMotorTest` bin, which exits after 3000 ms (This is used for CI servers).
