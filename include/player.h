#ifndef PLAYER
#define PLAYER

#include "mains.h"
#include "motor.h"

class Player : public Asset
{
    public:
        int Status;
    public:
        Player();
        ~Player();
        void HandleInput();
        void ShowMove();
};

Player::Player()
{
    HandleInput();
}

Player::~Player()
{

}

void Player::HandleInput()
{
    PollEvent();

    if (MainEvent.type == SDL_KEYDOWN) {
        switch (MainEvent.key.keysym.sym) {
            case SDLK_RIGHT: Velocity += DestR.w; break;
            case SDLK_LEFT: Velocity -= DestR.w; break;
        }
    }
    else if (MainEvent.type == SDL_KEYUP) {
        switch (MainEvent.key.keysym.sym) {
            case SDLK_RIGHT: Velocity -= DestR.w; break;
            case SDLK_LEFT: Velocity += DestR.w; break;
        }
    }

}

void Player::ShowMove()
{
    //DestR.w += Velocity;
    //SDL_RenderCopy(GetRenderer(), AssetOpt, NULL, &DestR);
    //SDL_RenderPresent(GetRenderer());
}

#endif
