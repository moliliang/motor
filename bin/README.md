# bin
## motorwork
`motorwork` is just a shell script to install dependences needed to build tools
needed to work on [motor](https://github.com/0x1A/motor).

### What it does
`motorwork` checks for dependencies installed on the system (currently supports Debian, Ubuntu, and Arch) and installs needed dependencies into `/usr/local/`. After dependencies are installed it pulls the latest stable needed libraries, compiles, and installs them.

### Current Dependencies (Installed through native package managers)
Debian/Ubuntu:
* `curl, build-essential, cmake`
* `build-deps` for `libsdl2 libsdl2-mixer libsdl2-image libsdl2-ttf` (Debian)
* `build-deps` for `libsdl2-2.0-0 libsdl2-mixer-2.0-0 libsdl2-image-2.0-0 libsdl-ttf2.0-0 libphysfs1` (Ubuntu)

Arch has dependencies solved through `base-devel`.

#### Dependencies pulled and built from source
* [SDL2 2.X.X](http://libsdl.org/download-2.0.php) (pulled from my git SDL repo)
* [SDL2-image 2.0.0](http://www.libsdl.org/projects/SDL_image/)
* [SDL2-ttf 2.0.1](http://www.libsdl.org/projects/SDL_ttf/)
* [SDL2-mixer 2.0.0](http://www.libsdl.org/projects/SDL_mixer/)
* [PhysicsFS 2.0.3](https://icculus.org/physfs/)
* [libyaml 0.1.4](http://pyyaml.org/wiki/LibYAML)

## Usage
To install dependencies run `./motorwork --install` and to uninstall run `./motorwork --uninstall`.
Note: Running uninstall does not delete the sources pulled to `~/.src`.

## clean
`clean` is a shell script that cleans up the compile of Motor after running `make`. Is run through `make clean`.

## memcheck
`memcheck` runs [valgrind](http://valgrind.org/) to check for memory leaks. Outputs to a log file `memcheck.txt`.
To run memcheck run `./bin/memcheck <argument>` where argument is what you want to check for memory leaks.
